
import { useState } from "react";
import Button from "./Button";

export default function Keyboard({ updateCurrentNumber, doOperation, clear, setCurrentOperation }) {

    var [numberButtons] = useState(() => {
        var arr = [];
        for(let i = 1; i < 10; i++) {
            arr.push(i);
            if(i == 9) {
                arr.push(0);
            }
        }
        return arr;
    })

    var [operationButtons] = useState(['+', '-', '*', '/']);

    return (

        <div className="keyboard">
            {numberButtons.map(button => {
                return (
                    <Button key={button} id={button} handleClick={() => updateCurrentNumber(button)} />
                )
            })}
            {operationButtons.map(button => {
                return <Button key={button} id={button} handleClick={() => setCurrentOperation(button)} />
            })}
            
            <button id="=" className="btn" onClick={doOperation}>=</button>
            <button id="clear" className="btn" onClick={clear}>CLEAR</button>
        </div>

    )
}
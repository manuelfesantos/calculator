import React from "react";
import { useState } from "react";
import Display from "./Display";
import Keyboard from "./Keyboard";

export default function App() {

  var [result, setResult] = useState(0);
  var [currentNumber, setCurrentNumber] = useState('');
  var [operation, setOperation] = useState('');

  function updateCurrentNumber(number) {
    setCurrentNumber(currentNumber => Number(currentNumber.toString() + number.toString()))
  }

  function setCurrentOperation(newOperation)  {
    if(operation.length > 0 && currentNumber !== '') doOperation();
    else {
      setResult(currentResult => currentResult === 0 ? currentNumber : currentNumber !== '' ? currentNumber : currentResult);
    }
    setOperation(currentOperation => newOperation);
    setCurrentNumber(number => '');
  }

  function doOperation() {
    switch(operation) {
      case '': setResult(currentResult => currentNumber.length > 0 ? currentNumber : 0); break;
      case '+': setResult(currentResult => Number(currentResult) + Number(currentNumber)); break;
      case '-': setResult(currentResult => Number(currentResult) - Number(currentNumber)); break;
      case '*': setResult(currentResult => Number(currentResult) * Number(currentNumber)); break;
      case '/': setResult(currentResult => Number(currentResult) / Number(currentNumber)); break;
      default: setResult(currentResult => 0); break;
    }
    setCurrentNumber(number => '');
    setOperation(currentOperation => '');
  }

  function clear() {
    setResult(currentResult => 0);
    setCurrentNumber(number => '')
    setOperation(currentOperation => '')
  }

  return (
    <div className="calculator">
      <Display result={result} operation={operation} currentNumber={currentNumber} setCurrentNumber={setCurrentNumber}/>
      <Keyboard updateCurrentNumber={updateCurrentNumber} doOperation={doOperation} clear={clear} setCurrentOperation={setCurrentOperation}/>
    </div>
  )
}
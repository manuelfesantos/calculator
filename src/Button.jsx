export default function Button({ id, handleClick }) {
    return (
        <button id={id} className="btn" onClick={handleClick}>{id}</button>
    )
}
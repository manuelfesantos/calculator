export default function Display({ result, operation, currentNumber, setCurrentNumber }) {

function updateNumber(value) {
  console.log(typeof value);
  if(!isNaN(value) || (value.slice(-1) === '.' && value.split('').filter(str => str === '.').length === 1)) {
    setCurrentNumber(value);
  }
}

    return (
      <div className="display">
        <div className="result">
          <span className="resultNumber">{result}</span>
          <span className="operation">{operation}</span>
          </div>
        <input value={currentNumber} onChange={e => updateNumber(e.target.value)} placeholder="" className="currentNumber" />
      </div>
    )
}